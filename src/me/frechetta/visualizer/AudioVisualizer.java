package me.frechetta.visualizer;

import java.io.File;
import java.util.ArrayList;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

import me.frechetta.visualizer.visualizations.Visualization;
import me.frechetta.visualizer.visualizations.grid.Grid_SrcMid_BassMid;

import org.farng.mp3.AbstractMP3Tag;
import org.farng.mp3.MP3File;
import org.lwjgl.input.Mouse;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.audio.AudioDevice;
import com.badlogic.gdx.audio.analysis.KissFFT;
import com.badlogic.gdx.audio.io.Mpg123Decoder;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;

/**
 * AudioVisualizer is the main class for the whole system. It facilitates the complete process of the Visualizer.
 * This process is as follows:
 * <ul>
 * <li>Load the mp3 file to visualize and play
 * <li>Go through the song, decoding each frame to raw data samples
 * <li>Write samples to AudioDevice
 * <li>Transform samples data into spectrum data
 * <li>Visualize spectrum data and draw visualization to screen
 * </ul>
 * 
 * @author Eric
 */
public class AudioVisualizer extends Game
{
	public static int WIDTH = 1600;
	public static int HEIGHT = 900;
	
	public static final int SAMPLE_SIZE = 2048;
	
	public static boolean fullscreen = false;
	
	private OrthographicCamera camera;
	private SpriteBatch batch;
	private ShapeRenderer shapeRenderer;
	
	private ArrayList<Sample> samples;
	private ArrayList<float[]> spectrum;
	
	private KissFFT fft;
	private Visualization visualization;
	private Mpg123Decoder decoder;
	private AudioDevice device;
	
	private Thread playbackThread;
	
	private boolean paused = true;
	private boolean playing = false;
	private boolean playSound = true;
	
	private boolean opening = false;
	
	private boolean closing = false;
	
	private JFileChooser fileChooser;
	
	private BitmapFont font;
	
	private String songDesc = "";
	
	private int songDescX;
	private int songDescY;
	
	private int songPosition = 0;

	
	@Override
	public void create()
	{
		if (fullscreen) Mouse.setGrabbed(true);
		
		// libGDX graphics
		camera = new OrthographicCamera();
		camera.setToOrtho(false, WIDTH, HEIGHT);
		batch = new SpriteBatch();
		shapeRenderer = new ShapeRenderer();
		
		samples = new ArrayList<Sample>();
		spectrum = new ArrayList<float[]>();
		
		samples.add(new Sample(new short[SAMPLE_SIZE]));
		spectrum.add(new float[SAMPLE_SIZE]);
		
		fft = new KissFFT(SAMPLE_SIZE);
		
		// create visualization
		visualization = new Grid_SrcMid_BassMid(batch, spectrum);
		
		fileChooser = new JFileChooser();
		fileChooser.setFileFilter(new FileNameExtensionFilter("MP3 file", "mp3"));
		
		font = new BitmapFont();
		
		openFile();
	}
	
	
	/**
	 * Opens a dialog to open a file from the file system.
	 */
	public void openFile()
	{
		opening = true;
		
		new Thread(new Runnable()
		{
			@Override
			public void run()
			{
				int returnVal = fileChooser.showOpenDialog(null);
				
				if (returnVal == JFileChooser.APPROVE_OPTION)
				{
					String path = fileChooser.getSelectedFile().getAbsolutePath();
					
					try
					{
						MP3File song = new MP3File(new File(path));
						
						AbstractMP3Tag tag = null;
						
						if (song.hasID3v1Tag())
						{
							tag = song.getID3v1Tag();
						}
						else if (song.hasID3v2Tag())
						{
							tag = song.getID3v2Tag();
						}
						
						if (tag != null)
						{
							songDesc = tag.getLeadArtist() + " - " + tag.getSongTitle();
						}
						
					}
					catch (Exception e)
					{
						e.printStackTrace();
					}
					
					if (songDesc.equals(" - "))
					{
						songDesc = "Unknown";
					}
					
					songDescX = WIDTH - 20 - (int)font.getBounds(songDesc).width;
					songDescY = HEIGHT - 20 - (int)font.getBounds(songDesc).height;
					
					play(path);
				}
				
				opening = false;
			}
		}).start();
	}
	
	/**
	 * Creates and starts a thread for playback for the song specified by path. 
	 * The playback thread manages decoding, writing to the audio device, and transforming 
	 * with Fast Fourier Transform (FFT).
	 * 
	 * @param path
	 */
	public void play(String path)
	{
		songPosition = 0;
		
		paused = true;
		playing = false;
		
		if (playbackThread != null) while (playbackThread.isAlive()) {}
		
		FileHandle file = new FileHandle(path);
		
		// declaring objects
		decoder = new Mpg123Decoder(file);
		device = Gdx.audio.newAudioDevice(decoder.getRate(), decoder.getChannels() == 1 ? true : false);
		
		samples.clear();
		spectrum.clear();
		
		samples.add(new Sample(new short[SAMPLE_SIZE]));
		spectrum.add(new float[SAMPLE_SIZE]);
		
		songPosition = 0;
		
		int readSamples = 0;
		
		while ((readSamples = decoder.readSamples(samples.get(samples.size() - 1).getSamples(), 0, SAMPLE_SIZE)) > 0)
		{
			fft.spectrum(samples.get(samples.size() - 1).getSamples(), spectrum.get(spectrum.size() - 1));
			samples.get(samples.size() - 1).readSamples = readSamples;
			
			samples.add(new Sample(new short[SAMPLE_SIZE]));
			spectrum.add(new float[SAMPLE_SIZE]);
		}

		// start a thread for playback
		playbackThread = new Thread(new Runnable()
		{
			@Override
			public void run()
			{
				while (playing)
				{
					if (songPosition >= samples.size() - 1)
					{
						paused = true;
						playing = false;
						//songPosition = 0;
					}
					
					if (!paused && playSound)
					{
						device.writeSamples(samples.get(songPosition).getSamples(), 0, samples.get(songPosition).readSamples);
						
						songPosition++;
					}
				}
				
				visualization.clear();
			}
		});
		
		paused = false;
		playing = true;
		playbackThread.setDaemon(true);
		playbackThread.start();
	}

	
	@Override
	public void render()
	{
		// clear screen each frame
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		
		// update camera
		camera.update();
		batch.setProjectionMatrix(camera.combined);
		shapeRenderer.setProjectionMatrix(camera.combined);

		batch.begin();
		
		visualization.visualize(spectrum.get(songPosition));
		
		if (playing) font.draw(batch, songDesc, songDescX, songDescY);
		
		batch.end();
		
		shapeRenderer.begin(ShapeType.Filled);
		
		shapeRenderer.setColor(0.1f, 0.1f, 0.1f, 1.0f);
		shapeRenderer.rect(0, HEIGHT - 5, WIDTH, 5);
		
		int barWidth = (int)((float)songPosition / spectrum.size() * WIDTH);
		
		shapeRenderer.setColor(0.2f, 0.2f, 0.25f, 1.0f);
		shapeRenderer.rect(0, HEIGHT - 5, barWidth < 5 ? 5 : barWidth, 5);
		
		shapeRenderer.end();
		
		pollInput();
	}
	
	
	@Override
	public void resize(int width, int height)
	{
		WIDTH = width;
		HEIGHT = height;
		camera.setToOrtho(false, WIDTH, HEIGHT);
		
		songDescX = WIDTH - 20 - (int)font.getBounds(songDesc).width;
		songDescY = HEIGHT - 20 - (int)font.getBounds(songDesc).height;
		
		visualization.resize();
	}
	
	
	/**
	 * Detects hotkey presses and performs the appropriate actions.
	 */
	public void pollInput()
	{
		if (Gdx.input.isKeyPressed(Keys.O) && !opening)
		{
			openFile();
		}
		else if (Gdx.input.isKeyPressed(Keys.P) && !paused)
		{
			paused = true;
		}
		else if (Gdx.input.isKeyPressed(Keys.R) && paused)
		{
			paused = false;
		}
		else if (Gdx.input.isKeyPressed(Keys.ESCAPE) && !closing)
		{
			closing = true;
			
			dispose();
		}
		
		if (playing && Gdx.input.isKeyPressed(Keys.LEFT))
		{
			songPosition -= 6;
			if (songPosition < 0) songPosition = 0;
			playSound = false;
		}
		else if (playing && Gdx.input.isKeyPressed(Keys.RIGHT))
		{
			songPosition += 6;
			if (songPosition >= spectrum.size()) songPosition = spectrum.size() - 1;
			playSound = false;
		}
		else
		{
			playSound = true;
		}
	}

	
	@Override
	public void dispose()
	{
		device.dispose();
		
		// synchronize with the thread
		paused = true;
		playing = false;
		
		if (playbackThread != null) while (playbackThread.isAlive()) {}
		
		// exit program
		Gdx.app.exit();
	}
	
	
	
	public static void main(String[] args)
	{
		if (args.length == 1 && args[0].equals("-f"))
		{
			fullscreen = true;
		}
		
		LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
		cfg.title = "AudioVisualizer";
		cfg.useGL20 = false;
		
		if (fullscreen)
		{
			System.setProperty("org.lwjgl.opengl.Window.undecorated", "true");
			cfg.resizable = false;
			WIDTH = LwjglApplicationConfiguration.getDesktopDisplayMode().width;
			HEIGHT = LwjglApplicationConfiguration.getDesktopDisplayMode().height;
		}
		else
		{
			cfg.resizable = true;
			WIDTH = 1024;
			HEIGHT = 768;
		}
		
		cfg.width = WIDTH;
		cfg.height = HEIGHT;
		
		new LwjglApplication(new AudioVisualizer(), cfg);
	}
}