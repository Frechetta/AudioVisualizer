package me.frechetta.visualizer;

public class Sample
{
	public int readSamples;
	private short[] samples;
	
	
	public Sample(short[] samples)
	{
		this.samples = samples;
	}
	
	public short[] getSamples()
	{
		return samples;
	}
}
