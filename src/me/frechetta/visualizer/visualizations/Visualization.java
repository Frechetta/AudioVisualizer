package me.frechetta.visualizer.visualizations;

import java.util.ArrayList;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Visualization represents how the spectrum data of a song is visualized.
 * 
 * @author Eric
 */
public abstract class Visualization
{
	protected SpriteBatch batch;
	protected ArrayList<float[]> spectrum;
	protected int[] displayData;
	
	
	/**
	 * Constructor
	 * 
	 * @param _batch
	 * @param _spectrum
	 */
	public Visualization(SpriteBatch batch, ArrayList<float[]> spectrum)
	{
		this.batch = batch;
		this.spectrum = spectrum;
	}
	
	
	/**
	 * Visualizes spectrum data and draws it to the screen.
	 */
	public abstract void visualize(float[] spectrum);
	
	
	public abstract void resize();
	
	
	/**
	 * Sets visualization data to 0.
	 */
	public void clear()
	{
		for (int i = 0; i < spectrum.size(); i++)
		{
			for (int j = 0; j < spectrum.get(i).length; j++)
			{
				spectrum.get(i)[j] = 0;
			}
		}
		
		/*for (int i = 0; i < spectrum.length; i++)
		{
			spectrum[i] = 0;
		}*/
	}
}
