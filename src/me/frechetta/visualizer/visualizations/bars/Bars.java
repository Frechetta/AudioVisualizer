package me.frechetta.visualizer.visualizations.bars;

import java.util.ArrayList;

import me.frechetta.visualizer.AudioVisualizer;
import me.frechetta.visualizer.visualizations.Visualization;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Bars visualizes the spectrum data using solid bars.
 * 
 * @author Eric
 */
public abstract class Bars extends Visualization
{
	protected Texture colors;
	
	protected float[] topValues = new float[AudioVisualizer.SAMPLE_SIZE];
	protected float[] maxValues = new float[AudioVisualizer.SAMPLE_SIZE];
	
	protected int NUM_BARS = 128;
	protected int numSamplesPerBar = (AudioVisualizer.SAMPLE_SIZE / NUM_BARS);
	
	protected float barWidth = ((float) AudioVisualizer.WIDTH / (float) NUM_BARS);
	
	protected float FALLING_SPEED = (1.0f / 3.0f);
	
	
	/**
	 * Constructor
	 * 
	 * @param batch
	 * @param spectrum
	 */
	public Bars(SpriteBatch batch, ArrayList<float[]> spectrum)
	{
		super(batch, spectrum);
		
		colors = new Texture(Gdx.files.internal("colors-borders.png"));
		
		displayData = new int[NUM_BARS / 4];
	}
	
	
	/**
	 * Draws a single bar to the screen.
	 * 
	 * @param i
	 * @param barNum
	 */
	public void drawBar(int i, int barNum, float[] spectrum)
	{
		if (avg(barNum, numSamplesPerBar, spectrum) > maxValues[barNum])
		{
			maxValues[barNum] = avg(barNum, numSamplesPerBar, spectrum);
		}

		if (avg(barNum, numSamplesPerBar, spectrum) > topValues[barNum])
		{
			topValues[barNum] = avg(barNum, numSamplesPerBar, spectrum);
		}

		// drawing spectrum (in blue)
		batch.draw(colors, i * barWidth, 0, barWidth, scale(avg(barNum, numSamplesPerBar, spectrum)), 0, 0, 16, 5, false, false);
		// drawing top values (in red)
		batch.draw(colors, i * barWidth, scale(topValues[barNum]), barWidth, 4, 0, 5, 16, 5, false, false);
		// drawing max values (in yellow)
		batch.draw(colors, i * barWidth, scale(maxValues[barNum]), barWidth, 2, 0, 10, 16, 5, false, false);

		
		topValues[barNum] -= FALLING_SPEED;
	}
	
	/**
	 * Scales a bar's value (x) to fit the screen.
	 * 
	 * @param x
	 * @return scaled x
	 */
	private float scale(float x)
	{
		return x / 256 * AudioVisualizer.HEIGHT * 1.5f;
	}

	/**
	 * Computes the average sample data for a certain bar.
	 * 
	 * @param barNum
	 * @param numSamplesPerBar
	 * @return average sample data for a certain bar
	 */
	protected float avg(int barNum, int numSamplesPerBar, float[] spectrum)
	{
		int sum = 0;
		
		for (int i = 0; i < numSamplesPerBar; i++)
		{
			sum += spectrum[barNum + i];
		}

		return (float) (sum / numSamplesPerBar);
	}
	
	
	@Override
	public void clear()
	{
		super.clear();
		
		for (int i = 0; i < AudioVisualizer.SAMPLE_SIZE; i++)
		{
			topValues[i] = 0;
			maxValues[i] = 0;
		}
	}
	
	
	public void resize()
	{
		barWidth = ((float) AudioVisualizer.WIDTH / (float) NUM_BARS);
	}
}
