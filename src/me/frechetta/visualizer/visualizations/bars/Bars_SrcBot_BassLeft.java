package me.frechetta.visualizer.visualizations.bars;

import java.util.ArrayList;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Bars_SrcBot_BassLeft is a type of Bars visualization 
 * with the source of the bars at the bottom of the screen 
 * and the bass on the left.
 * 
 * @author Eric
 */
public class Bars_SrcBot_BassLeft extends Bars
{
	/**
	 * Constructor
	 * 
	 * @param batch
	 * @param spectrum
	 */
	public Bars_SrcBot_BassLeft(SpriteBatch batch, ArrayList<float[]> spectrum)
	{
		super(batch, spectrum);
	}
	
	
	@Override
	public void visualize(float[] spectrum)
	{
		float sum = 0;
		
		for (int i = 0; i < NUM_BARS; i++)
		{
			int barNum = i;
			
			sum += avg(barNum, numSamplesPerBar, spectrum);
			
			if (i % (4 - 1) == 0)
			{
				displayData[i / 4] = (int)(sum / 4);
				sum = 0;
			}
			
			drawBar(i, barNum, spectrum);
		}
	}
}
